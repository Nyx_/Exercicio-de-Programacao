#include <stdio.h>
#include <stdlib.h>

float volumeEsfera (float x);

int main() {
  float raio;
  printf("Digite o raio:\n");
  scanf("%f",&raio);
  printf("O volume e' : %.2f\n", volumeEsfera(raio));

  return 0;
}

float volumeEsfera (float x){
  return (4/(3 * 3.1414592 ))* x * x * x;
}
