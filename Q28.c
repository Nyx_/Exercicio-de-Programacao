#include <stdio.h>
#include <stdlib.h>

int potencia(int x, int y);

int main() {
  int n1,n2;

  printf("Digite 2 valores:\n");
  scanf("%d %d",&n1,&n2);

  printf("A potencia e': %d\n", potencia(n1,n2));

  return 0;
}

int potencia(int x, int y){
    int i,result = 1;
    for (i = 0; i < y; i++)
        result *= x;
    return result;
}
