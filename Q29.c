#include <stdio.h>
#include <stdlib.h>

int somatoria(int x);

int main() {
  int n;

  printf("Digite 1 valores:\n");
  scanf("%d",&n);
  printf("A somatoria e': %d\n", somatoria(n));

  return 0;
}

int somatoria(int x){
    if(x == 1)
      return 1;
    else
      return (x + somatoria(x - 1));
}
