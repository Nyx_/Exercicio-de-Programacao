#include <stdio.h>
#include <stdlib.h>

void numReal (float n, int *x, float *y);
int main() {
  float num,fracao;
  int inteiro;
  scanf("%f",&num);
  numReal(num, &inteiro,&fracao);
  printf("%.2f: %d e %.2f\n",num,inteiro,fracao);

  return 0;
}

void numReal (float n, int *x, float *y){
    *x = (int)n;
    *y = n - *x;
}
