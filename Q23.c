#include <stdio.h>
#include <stdlib.h>

float operacao (float x, float y,char s);

int main() {
  float n1,n2;
  char simbol;
  printf("Digite dois numeros e um simbolo:\n");
  scanf("%f %f %c",&n1,&n2,&simbol);
  printf("O resultado e' : %.2f\n", operacao(n1,n2,simbol));


  return 0;
}

float operacao (float x, float y,char s){
  if (s == '+')
      return x + y;
  else if (s == '-')
      return x - y;
  else if (s == '*')
      return x * y;
  else if (s == '/')
      return x / y;


}
