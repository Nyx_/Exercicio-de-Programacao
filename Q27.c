#include <stdio.h>
#include <stdlib.h>

float media(float n1 , float n2,float n3,char tipo);

int main() {
  float nota1 , nota2, nota3;
  char type;

  printf("Digite as 3 notas e o tipo da media(A ou P):\n");
  scanf("%f %f %f %c",&nota1,&nota2,&nota3,&type);

  printf("A media e %.2f\n", media(nota1 , nota2, nota3,type));


  return 0;
}

float media(float n1 , float n2,float n3,char tipo){
      if (tipo == 'A'|| tipo == 'a')
          return (n1 + n2 + n3)/3;
      else if (tipo == 'P' || tipo == 'p')
          return ((n1 * 5)+(n2 * 3)+(n3 * 2))/(5+3+2) ;

}
