#include <stdio.h>
#include <stdlib.h>

float somaArray (int n, float x[]);

int main() {
  float array[10];
  printf("Digite os numeros:\n");
  for (int i = 0; i < 10; i++) {
    scanf("%f",&array[i]);

  }
  printf("A soma e' : %.2f\n", somaArray(10,array));

  return 0;
}

float somaArray (int n,float x[]){
    float result = 0;
    for (int i = 0 ; i < n; i++)
        result += x[i];

  return result;
}
