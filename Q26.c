#include <stdio.h>
#include <stdlib.h>

int impar(int x);

int main() {
  int num;

    printf("Digite 1 numero:\n");
    scanf("%d",&num );

    if (impar(num))
       printf("%d e' impar\n",num);
    else
       printf("%d nao e' impar\n",num);

  return 0;
}

int impar(int x){
      if(x % 2 != 0)
         return 1;
      else
         return 0;

}
