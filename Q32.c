#include <stdio.h>
#include <stdlib.h>

float AREAeVOLUME (float r, float *vo, float *ar);
int main() {
  float raio, vo, ar;
  scanf("%f",&raio);
  AREAeVOLUME(raio, &vo, &ar);
  printf("%.2f %.2f\n",vo,ar);


  return 0;
}

float AREAeVOLUME (float r, float *vo, float *ar){

  *vo = (4 * 3.1414592 * r * r * r)/3;

  *ar = 4*3.1414592 * r * r;
}
